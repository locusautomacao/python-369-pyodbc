FROM python:3.6.9-slim

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    make \
    git \
    locales locales-all \
    apt-utils apt-transport-https debconf-utils gcc gnupg2 curl build-essential unixodbc-dev\
    && rm -rf /var/lib/apt/lists/*

# adding custom MS repository
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list

# install SQL Server drivers
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y msodbcsql17 mssql-tools
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
RUN /bin/bash -c "source ~/.bashrc"

RUN locale-gen pt_BR.UTF-8

RUN python -m pip install --upgrade pip

ENV LANG pt_BR.UTF-8

ENV LANGUAGE pt_BR:pt

ENV LC_ALL pt_BR.UTF-8